// Soal No. 1 (Array to Object)

var now = new Date();
var thisYear = now.getFullYear();

function arrayToObject(arr) {
    var n = arr.length;
    for(var i=0; i<n; i++){
        var peopleObj = {
            firstName : arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: thisYear - arr[i][3]
        } 
        if(!arr[i][3] || arr[i][3]>thisYear ) peopleObj.age = "Invalid Birth Year";
        var consoleText = (i+1) +". "+ peopleObj.firstName + " " + peopleObj.lastName + ": "; 
        console.log(consoleText);
        console.log(peopleObj);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people) ;
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2) ;

// Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else if(money<50000){
        return "Mohon maaf, uang tidak cukup";
    }
    var initialMoney = money;
    var listPurchased = [];
    while(money>=50000 && listPurchased.indexOf('Casing Handphone') < 0){
        if(money>=1500000 && listPurchased.indexOf('Sepatu Stacattu') < 0) {
            listPurchased.push('Sepatu Stacattu');
            money -= 1500000;
        } else if(money >= 500000 && listPurchased.indexOf('Baju Zoro') < 0){
            listPurchased.push('Baju Zoro');
            money -= 500000;
        } else if(money >= 250000 && listPurchased.indexOf('Baju H&N') < 0){
            listPurchased.push('Baju H&N');
            money -= 250000;
        } else if(money >= 175000 && listPurchased.indexOf('Sweater Uniklooh') < 0){
            listPurchased.push('Sweater Uniklooh');
            money -= 175000;
        } else if(listPurchased.indexOf('Casing Handphone') < 0){
            listPurchased.push('Casing Handphone');
            money -= 50000;
        }
    }
    var memberObj = {
        memberID: memberId,
        money: initialMoney,
        listPurchased: listPurchased,
        changeMoney: money,
    };
    return memberObj;
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));

  console.log(shoppingTime('82Ku8Ma742', 170000));

  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var n = arrPenumpang.length;
    var result = [];
    for(var i = 0; i<n; i++){
        var angkotObj = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: 0
        }
        var ongkos = rute.indexOf(angkotObj.tujuan) - rute.indexOf(angkotObj.naikDari);
        angkotObj.bayar = ongkos*2000;
        result.push(angkotObj);
    }
    return result;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]