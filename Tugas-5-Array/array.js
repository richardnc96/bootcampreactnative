// Soal No. 1 (Range) 

function range(startNum, finishNum){
    var result = [];
    if(startNum < finishNum){
        for(var i=startNum; i<=finishNum; i++){
            result.push(i);
        }
        return result;
    }
    else if(startNum > finishNum){
        for(var i=startNum; i>=finishNum; i--){
            result.push(i);
        }
        return result;
    } else{
        return -1;
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)

function rangeWithStep(startNum, finishNum, step){
    var result2 = [];
    if(startNum < finishNum){
        var i = startNum;
        while(i<=finishNum){
            result2.push(i);
            i+=step;
        }
        return result2;
    }
    else if(startNum > finishNum){
        var i = startNum;
        while(i>=finishNum){
            result2.push(i);
            i-=step;
        }
        return result2;
    } else{
        return -1;
    }
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)

function sum(startNum, finishNum, step){
    var result3 = 0;
    if(!startNum) return 0;
    if(!finishNum) return startNum;
    if(!step) step=1;
    if(startNum < finishNum){
        var i = startNum;
        while(i<=finishNum){
            result3+= i;
            i+=step;
        }
        return result3;
    }
    else if(startNum > finishNum){
        var i = startNum;
        while(i>=finishNum){
            result3+= i;
            i-=step;
        }
        return result3;
    }
    else{
        return -1;
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input){
    var n = input.length;

    for(var i=0; i<n; i++){
    console.log("Nomor ID: "+ input[i][0]);
    console.log("Nama Lengkap: "+ input[i][1]);
    console.log("TTL: "+ input[i][2] + " " + input[i][3]);
    console.log("Hobi: "+ input[i][4]);
    console.log(" ");
    }
}

dataHandling(input);

// Soal No. 5 (Balik Kata)

function balikKata(input){
    var strArray = [];
    var reverseArray = [];
    var n = input.length;

    for(var i=0; i<n; i++){
        strArray.push(input.charAt(i));
    }
    for(var j=n; j>0; j--){
        reverseArray.push(strArray[j-1]);
    }
    return reverseArray.join("");
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)

function dataHandling2(input){
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    // Ubah Array
    input[1] = input[1] + "Elsharawy";
    input[2] = "Provinsi " + input[2];
    console.log(input);

    // Switch Case Bulan
    var ttl = input[3].split("/");
    var ttlJoin = ttl.join("-");
    var bulanlahir = Number(ttl[1]);
    var bulan = "";
    switch(bulanlahir) {
        case 1:   { bulan = "Januari"; break; }
        case 2:   { bulan = "Februari"; break; }
        case 3:   { bulan = "Maret"; break; }
        case 4:   { bulan = "April"; break; }
        case 5:   { bulan = "Mei"; break; }
        case 6:   { bulan = "Juni"; break; }
        case 7:   { bulan = "Juli"; break; }
        case 8:   { bulan = "Agustus"; break; }
        case 9:   { bulan = "September"; break; }
        case 10:   { bulan = "Oktober"; break; }
        case 11:   { bulan = "November"; break; }
        case 12:   { bulan = "Desember"; break; }
        default:  { bulan = "invalid"; break}
    }
    console.log(bulan);

    // Sorting Descend Tanggal Lahir
    ttl.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(ttl);

    // Join Tanggal Lahir
    console.log(ttlJoin);

    // Nama 15 Karakter
    var nama = input[1];
    var l = nama.length;
    var arrNama = [];
    for(var k=0; k<l; k++){
        arrNama.push(nama.charAt(k));
    }
    console.log(arrNama.slice(0,14).join(""));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
