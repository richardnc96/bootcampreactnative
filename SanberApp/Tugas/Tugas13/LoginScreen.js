import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';
export default function LoginScreen() {

    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image
                    style={styles.logo}
                    source={require('./asset/icon.png')}
                />
                <Text style={styles.titleName}>PORTO DEV</Text>
            </View>
            <View style={styles.content}>
                <SafeAreaView>
                    <TouchableOpacity style={styles.containerInput}>
                        <View >
                            <TextInput style={styles.divinput} placeholder="username/email"/>
                            <TextInput style={styles.divinput} placeholder="password"/>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.containerInput}>
                        <View style={styles.buttonLogin}>
                            <Text style={styles.buttonText}>Login</Text>
                        </View>
                        <Text>or Register</Text>
                    </TouchableOpacity>
                </SafeAreaView>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE',

    },
    logoContainer:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 100
    },
    logo:{
        height: 75,
        width: 75
    },
    titleName:{
        color: 'black',
        fontSize: 24,
        textAlign: 'center',
        fontWeight: "900"
    },
    containerInput:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLogin:{
        backgroundColor: "blue",
        width: 200,
        padding: 20,
        margin: 10,
        borderRadius: 15
    },
    buttonText:{
        color: 'white',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 18
    },
    divinput:{
        padding: 15,
        margin: 10,
        backgroundColor: 'white',
        width: 200
    },
    nameMessage:{
        paddingLeft: 15
    }

})