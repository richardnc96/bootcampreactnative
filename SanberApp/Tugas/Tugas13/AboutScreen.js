import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';
export default function AboutScreen() {

    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Text style={styles.titleName}>ABOUT ME</Text>
                <Image
                    style={styles.picture}
                    source={require('./asset/foto.jpg')}
                />
                <Text style={styles.textName}>Richard Nathaniel Chandra</Text>
            </View>
            <View style={styles.content}>
                <SafeAreaView>
                    <View style={{borderBottomColor: 'black', borderBottomWidth: 2}}>
                        <Text style={styles.heading}>PORTFOLIO</Text>
                    </View>
                    <TouchableOpacity style={{flexDirection: 'row',justifyContent: 'space-between'}}>
                        <View >
                            <Image
                                style={styles.pictureicon}
                                source={require('./asset/github.png')}
                            />
                            <Text>richardnc96</Text>
                        </View>
                        <View >
                            <Image
                                style={styles.pictureicon}
                                source={require('./asset/gitlab.png')}
                            />
                            <Text>richardnc96</Text>
                        </View>
                        <View >
                            <Image
                                style={styles.pictureicon}
                                source={require('./asset/website.png')}
                            />
                            <Text>richardnc.com</Text>
                        </View>
                    </TouchableOpacity>
                    
                    <View style={{borderBottomColor: 'black', borderBottomWidth: 2}}>
                        <Text style={styles.heading}>REACH ME</Text>
                    </View>

                    <TouchableOpacity style={{flexDirection: 'row',justifyContent: 'space-between'}}>
                        <View >
                            <Image
                                style={styles.pictureicon}
                                source={require('./asset/facebook.png')}
                            />
                            <Text>Richard NC</Text>
                        </View>
                        <View >
                            <Image
                                style={styles.pictureicon}
                                source={require('./asset/twitter.png')}
                            />
                            <Text>richardnc96</Text>
                        </View>
                        <View >
                            <Image
                                style={styles.pictureicon}
                                source={require('./asset/instagram.png')}
                            />
                            <Text>richardnc96</Text>
                        </View>
                    </TouchableOpacity>
                </SafeAreaView>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEE',

    },
    logoContainer:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    picture:{
        height: 120,
        width: 120,
        borderRadius: 60
    },
    pictureicon:{
        height: 75,
        width: 75,
        marginTop: 30
    },
    titleName:{
        color: 'black',
        fontSize: 32,
        textAlign: 'center',
        fontWeight: "900"
    },
    textName:{
        color: 'black',
        fontSize: 18,
        textAlign: 'center',
        fontWeight: "700",
        marginTop: 20
    },
    heading:{
        textAlign: 'left',
        fontSize: 18,
        marginTop: 20,
        marginLeft: 10
    },
    containerInput:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonLogin:{
        backgroundColor: "blue",
        width: 200,
        padding: 20,
        margin: 10,
        borderRadius: 15
    },
    buttonText:{
        color: 'white',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 18
    },
    divinput:{
        padding: 15,
        margin: 10,
        backgroundColor: 'white',
        width: 200
    },
    nameMessage:{
        paddingLeft: 15
    }

})