// No. 1 Looping While 

console.log('LOOPING PERTAMA');
var x = 2;
while(x <= 20){
    console.log(x + ' - I love coding');
    x += 2;
}
console.log('LOOPING KEDUA');
var y=20;
while(y >= 2){
    console.log(y + ' - I will become a mobile developer');
    y -= 2;
}

// No. 2 Looping menggunakan for

for (var i=1; i<=20; i++){
    if(i%3 == 0 && i%2 != 0){
        console.log(i + ' - I Love Coding');
    }
    else if(i%2 != 0){
        console.log(i + ' - Santai');
    }
    else{
        console.log(i + ' - Berkualitas');
    }
}

// No. 3 Membuat Persegi Panjang #

for(var k=0; k<4; k++){
    var p = "";
    for(var l=0; l<8; l++){
        p = p+'#';
    }
    console.log(p);
}

// No. 4 Membuat Tangga 

for(var m=0; m<7; m++){
    var t = "";
    for(var n=0; n<m+1; n++){
        t = t+'#';
    }
    console.log(t);
}

// No. 5 Membuat Papan Catur

for(var a=0; a<8; a++){
    var c = "";
    if(a%2 == 0){
        for(var b=0; b<8; b++){
            if(b%2 == 0){
                c = c + " ";
            } else{
                c = c + "#";
            }
        }
    } else{
        for(var b=0; b<8; b++){
            if(b%2 == 0){
                c = c + "#";
            } else{
                c = c + " ";
            }
        }
    }
    console.log(c);
}